import { Component, OnInit } from '@angular/core';
import { Pokemon, trainer } from 'src/app/models/pokemon.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css'],
})
export class TrainerComponent implements OnInit {
  public tp: Array<any> = [];
  private temp: Array<any> = [];
  public name?: string = '';
  constructor(private router: Router) {}

  ngOnInit(): void {

    if( localStorage.getItem('mypokemons')){
    this.temp = JSON.parse(
      localStorage.getItem('mypokemons')!
    );

    }
     
    

    this.name = localStorage.getItem('user')!;

    if (!localStorage.getItem('user')) {  
      this.router.navigate(['/']);
    }

    this.temp.forEach((element) => { //loop through local storage array and put every object in a new array.
      this.tp.push(element);
    });
  }
}
